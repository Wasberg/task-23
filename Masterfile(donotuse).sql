USE world;

SELECT * FROM country;

/* 1. */
SELECT GROUP_CONCAT(Name) AS Countries, Region
FROM country
GROUP BY region
HAVING avg(population)>1000000;

/* 2. */
SELECT city.name AS CityName, city.population AS CityPopulation, country.name AS Country
FROM city
LEFT JOIN country
ON city.countrycode = country.code
ORDER BY country.name;

/* 3. */
SELECT GROUP_CONCAT(Language) AS Languages, Name AS Country, Region 
FROM countrylanguage 
LEFT JOIN country 
ON countrylanguage.countrycode = country.code
GROUP BY Name;