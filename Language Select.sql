SELECT GROUP_CONCAT(Language) AS Languages, Name AS Country, Region 
FROM countrylanguage 
LEFT JOIN country 
ON countrylanguage.countrycode = country.code
GROUP BY Name;