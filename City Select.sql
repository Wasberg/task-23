SELECT city.name AS CityName, city.population AS CityPopulation, country.name AS Country
FROM city
LEFT JOIN country
ON city.countrycode = country.code
ORDER BY country.name;