SELECT GROUP_CONCAT(Name) AS Countries, Region
FROM country
GROUP BY region
HAVING avg(population)>1000000;